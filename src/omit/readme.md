#### omit([paths])

This function creates an object composed of its own and inherited enumerable property paths of the input object that are not omitted.

Arguments:

> [paths]: The property paths to omit.

Returns:

> (Object): Returns the new object.

Example:

```shell
# input.json
{
    "a": 1,
    "b": "2",
    "c": 3
}

# cli
$ jq -L $PWD 'include "omit.min"; omit(["a", "c"])' input.json > output.json

# output.json
{
    "b": "2"
}
```

Minified:

```javascript
def omit(keys): to_entries | map(select(.key as $a | any(keys[]; . == $a ) |not )) | from_entries;
```

Beautified:

```javascript
def omit(keys):
    to_entries
    | map(
        select(
            .key as $a
            | any(keys[]; . == $a )
            | not ) )
    | from_entries;
```
