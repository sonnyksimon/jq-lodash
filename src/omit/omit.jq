def omit(keys): 
    to_entries 
    | map(
        select(
            .key as $a 
            | any(keys[]; . == $a ) 
            | not ) ) 
    | from_entries;