#### chunk([size])

Creates an array of elements split into groups the length of `size`. If array can't be split evenly, the final chunk will be the remaining elements.

Arguments:

> [size]: The length of each chunk

Returns:

> (Array): Returns the new array of chunks

Example:

```shell
# input.json
[
  "a",
  "b",
  "c",
  "d"
]

# cli
$ jq -L $PWD 'include "chunk.min"; chunk(2)' input.json > output1.json

# output1.json
[
  [
    "a",
    "b"
  ],
  [
    "c",
    "d"
  ]
]

# cli
$ jq -L $PWD 'include "chunk.min"; chunk(3)' input.json > output2.json

# output2.json
[
  [
    "a",
    "b",
    "c"
  ],
  [
    "d"
  ]
]
```

Minified:

```javascript
def chunk(size): [_nwise(size)];
```

Beautified:

```javascript
def chunk(size):
    [ _nwise(size) ];
```
