An attempt to implement [Lodash](http://lodash.com) in [jq](http://stedolan.github.io/jq/).

| lodash function | link                  |
| --------------- | --------------------- |
| `_.chunk`       | [chunk](./src/chunk/) |
| `_.omit`        | [omit](./src/omit/)   |
